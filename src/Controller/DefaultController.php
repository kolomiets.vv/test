<?php

namespace App\Controller;

use App\Document\User\BaseUser;
use App\Form\Type\User\FilterForm;
use App\Form\Type\User\UserType;
use App\Model\EntireUserInterface;
use App\Service\Manager\UserManager;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DefaultController
 *
 * @package App\Controller
 */
class DefaultController extends AbstractController
{
    /**
     * @todo save filter param to url
     *
     * List User Action (Dasboard)
     *
     * @Route("/", name="homepage")
     *
     * @param DocumentManager $manager
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function index(DocumentManager $manager, Request $request)
    {
        $params   = array_merge($request->query->all(), $request->request->all());
        $userList = $manager
            ->getRepository(BaseUser::class)
            ->userList($params);
        $form     = $this->createForm(
            FilterForm::class,
            isset($params['filter_form']) ? (new BaseUser())->setData($params['filter_form']) : null
        );

        return $this->render('dashboard.html.twig', [
            'title'        => 'User List',
            'users'        => $userList,
            'instance'     => BaseUser::class,
            'query_params' => $params,
            'form'         => $form->createView()
        ]);
    }

    /**
     * Create User Action
     *
     * @Route("/user/create", name="createUser")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     *
     * @param UserManager $userManager
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newUser(UserManager $userManager, Request $request, TranslatorInterface $translator)
    {
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            try {
                $userManager->create($user, true);
                $this->addFlash(
                    'success',
                    $translator->trans('User is successfully created.', [])

                );
            } catch (\Exception $exception) {
                $this->container->get('monolog.logger')->addError(
                    $exception->getMessage(),
                    [
                        'admin' => $this->getUser()->getUserName()
                    ]
                );
                $this->addFlash(
                    'error',
                    $translator->trans('Error occurred during user creation.', [])
                );
            }

            return new RedirectResponse($this->generateUrl('homepage'));
        }
        return $this->render('User/edit.html.twig', [
            'title' => $translator->trans('New User', []),
            'form'  => $form->createView()
        ]);
    }

    /**
     * Edit/Update User action
     *
     * @Route("/user/edit/{id}", name="editUser")
     * @ParamConverter("user", class="App\Document\User\BaseUser")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     *
     * @param UserInterface $user
     * @param Request $request
     * @param UserManager $userManager
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editUser(
        UserInterface $user,
        Request $request,
        UserManager $userManager,
        TranslatorInterface $translator
    ) {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            try {
                $userManager->update($user, true);
                $this->addFlash(
                    'success',
                    $translator->trans('User is successfully updated.', [])

                );
            } catch (\Exception $exception) {
                $this->container->get('monolog.logger')->addError(
                    $exception->getMessage(),
                    [
                        'admin' => $this->getUser()->getUserName()
                    ]
                );
                $this->addFlash(
                    'error',
                    $translator->trans('Error occurred during user updating.', [])

                );
            }

            return new RedirectResponse($this->generateUrl('homepage'));
        }
        return $this->render('User/edit.html.twig', [
            'title' => $translator->trans('Edit User', []),
            'form'  => $form->createView()
        ]);
    }

    /**
     * Edit/Update User action
     *
     * @Route("/user/remove/{id}", name="removeUser")
     * @ParamConverter("user", class="App\Document\User\BaseUser")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_SUPER_ADMIN')")
     *
     * @param UserInterface $user
     * @param Request $request
     * @param UserManager $userManager
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function removeUser(
        UserInterface $user,
        Request $request,
        UserManager $userManager,
        TranslatorInterface $translator
    ) {
        if($this->getUser() !== $user){
            $userManager->remove($user);
            $this->addFlash(
                'success',
                $translator->trans("User {$user} removed successfully.", [])
            );
        }else{
            $this->addFlash(
                'error',
                $translator->trans('User can\'t be removed by itself.', [])
            );
        }

        return new RedirectResponse($this->generateUrl('homepage'));
    }
}