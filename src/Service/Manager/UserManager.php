<?php

namespace App\Service\Manager;

use App\Event\User\UserEvent;
use Doctrine\ODM\MongoDB\DocumentManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManager as BaseUserManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class UserManager
 * @package App\Service\Manager
 */
class UserManager
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @var BaseUserManager
     */
    protected $fosManager;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * UserManager constructor.
     * @param BaseUserManager $fosManager
     * @param DocumentManager $documentManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        BaseUserManager $fosManager,
        DocumentManager $documentManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->fosManager = $fosManager;
        $this->dm         = $documentManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function update(UserInterface $user, $fireEvent = false): UserInterface
    {
        $this->fosManager->updateUser($user);
        if($fireEvent){
            $event = new UserEvent($user);
            $this->eventDispatcher->dispatch(UserEvent::UPDATE, $event);
        }
        return $user;
    }

    /**
     * @param UserInterface $user
     * @param bool $fireEvent
     * @return UserInterface
     */
    public function create(UserInterface $user, $fireEvent = false): UserInterface
    {
        $this->fosManager->updateUser($user);
        if($fireEvent){
            $event = new UserEvent($user);
            $this->eventDispatcher->dispatch(UserEvent::CREATION, $event);
        }
        return $user;
    }

    public function remove(UserInterface $user, $fireEvent = false)
    {
        $this->dm->remove($user);
        $this->dm->flush();
        if($fireEvent){
            $event = new UserEvent($user);
            $this->eventDispatcher->dispatch(UserEvent::REMOVE, $event);
        }
        return $user;
    }
}