<?php

namespace App\Service\Exception;

/**
 * Custom exception class can be used for render error pages
 *
 * Class ExpectedException
 * @package App\Service\Exception
 */
class ExpectedException extends \Exception
{
    /**
     * @var array
     */
    private $data;

    /**
     * ExpectedException constructor.
     * @param string $message
     * @param int $code
     * @param array $data
     */
    public function __construct($message = '', $code = 500, array $data = [])
    {
        parent::__construct($message, $code);
        $this->data = $data;
    }

    /**
     * @inheritdoc
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @inheritdoc
     */
    public function addData(string $key, string $value): void
    {
        $this->data[$key] = $value;
    }

    /**
     * Set error code
     *
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }
}