<?php

namespace App\Service\Twig;

use App\Model\RepositoryInterface;
use Symfony\Bundle\TwigBundle\DependencyInjection\TwigExtension;

/**
 * Helper class for render list
 *
 * Class ListTwigExtension
 * @package App\Service\Twig
 */
class ListTwigExtension extends TwigExtension implements \Twig_ExtensionInterface
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'list';
    }

    /**
     * @return array|\Twig_TokenParserInterface[]
     */
    public function getTokenParsers()
    {
        return array();
    }

    /**
     * @return array|\Twig_NodeVisitorInterface[]
     */
    public function getNodeVisitors()
    {
        return array();
    }

    /**
     * @return array|\Twig_Filter[]
     */
    public function getFilters()
    {
        return array();
    }

    /**
     * @return array|\Twig_Test[]
     */
    public function getTests()
    {
        return array();
    }

    /**
     * @return array
     */
    public function getOperators()
    {
        return [];
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            'order_link' => new \Twig_SimpleFunction(
                'order_link',
                [$this, 'orderLink']
            ),
        ];
    }

    /**
     * Build order link depens on url params
     *
     * @param string $method
     * @param string $currency
     *
     * @return string
     */
    public function orderLink(string $field, array $params): string
    {
        $out = [RepositoryInterface::SORT_KEY => $field];
        if (isset($params[RepositoryInterface::SORT_KEY]) && $field === $params[RepositoryInterface::SORT_KEY]) {
            $order = isset($params[RepositoryInterface::SORT_ORDER])
            && $params[RepositoryInterface::SORT_ORDER] == 'ASC' ? 'DESC' : 'ASC';

            $out[RepositoryInterface::SORT_ORDER] = $order;
        }
        return '?' . http_build_query($out);
    }
}