<?php

namespace App\Repository\User;

use App\Model\RepositoryInterface;
use Doctrine\ODM\MongoDB\DocumentRepository;

/***
 * Class UserRepository
 * @package App\Repository\User
 */
class UserRepository extends DocumentRepository implements RepositoryInterface
{
    /**
     * @todo pagination logic
     *
     * User List getter
     *
     * @param array|null $params
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function userList(array $params = null): array
    {
        $sort = $filter = [];

        if (isset($params[self::FILTER_FORM_KEY])) {
            $filter = array_filter($params[self::FILTER_FORM_KEY]);
            unset($filter['_token']);
        }
        if (isset($params[self::SORT_KEY])) {
            $sort = [$params[self::SORT_KEY] => isset($params[self::SORT_ORDER]) ? $params[self::SORT_ORDER] : 'ASC'];
        }

        return $this->paginate($this->findBy($filter, $sort), $params);
    }

    /**
     * Paginate users list
     *
     * @param array $items
     * @param array|null $params
     * @return array
     */
    protected function paginate(array $items, ?array $params = null)
    {
        return $items;
    }
}