<?php

namespace App\Event\User;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UserEvent
 * @package App\Event\User
 */
class UserEvent extends  Event
{
    const CREATION =  'app.event.user-created';
    const UPDATE   =  'app.event.user-created';
    const LOGIN    =  'app.event.user-login-after';
    const REMOVE   =  'app.event.user-remove';

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * UserEvent constructor.
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }


    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }
}