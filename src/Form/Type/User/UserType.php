<?php

namespace App\Form\Type\User;

use App\Document\User\BaseUser;
use App\Model\EntireUserInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserType
 * @package App\Form\Type\User
 */
class UserType extends AbstractType
{
    /**
     * User form builder
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('lastName', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('email', EmailType::class, ['attr' => ['class' => 'form-control']])
            ->add('username', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('plainPassword', RepeatedType::class, [
                'type'           => PasswordType::class,
                'attr'           => ['class' => 'form-control'],
                'label'          => 'Password',
                'required'       => $builder->getData() ? false : true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ])
            ->add('roles', ChoiceType::class, [
                'multiple' => true,
                'choices'  => [
                    UserInterface::ROLE_DEFAULT     => UserInterface::ROLE_DEFAULT,
                    EntireUserInterface::ROLE_ADMIN => EntireUserInterface::ROLE_ADMIN,
                    UserInterface::ROLE_SUPER_ADMIN => UserInterface::ROLE_SUPER_ADMIN,
                ],
                'attr'     => ['class' => 'form-control']
            ])
            ->add('enabled', CheckboxType::class, [
                'attr' => ['class' => 'form-control'],
            ])
            ->add('save', SubmitType::class, [
                'label' => $builder->getData() ? 'Update User' : 'Create User',
                'attr'  => ['class' => 'btn btn-primary']
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BaseUser::class,
        ]);
    }
}