<?php

namespace App\Form\Type\User;

use App\Document\User\BaseUser;
use App\Model\EntireUserInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * User List search form
 *
 * Class UserType
 * @package App\Form\Type\User
 */
class FilterForm extends UserType
{
    /**
     * Users list filter form builder
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $children = $builder->all();
        foreach ($children as $child) {
            if ($child instanceof FormBuilder) {
                $child->setRequired(false);
            }
        }
        $builder
            ->remove('enabled')
            ->remove('plainPassword')
            ->remove('save')
            ->remove('roles')
            ->add('save', SubmitType::class,
                [
                    'label' => 'Search',
                    'attr'  => ['class' => 'btn btn-primary']
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BaseUser::class,
        ]);
    }
}