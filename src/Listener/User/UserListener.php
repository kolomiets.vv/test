<?php

namespace App\Listener\User;

use App\Event\User\UserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UserListener
 * @package App\Listener\User
 */
class UserListener implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::CREATION => 'onUserCreated',
            UserEvent::LOGIN    => 'onUserLogin',
            UserEvent::UPDATE   => 'onUserUpdated',
            UserEvent::REMOVE   => 'onUserRemoved',
        ];
    }

    /**
     * @param UserEvent $event
     */
    public function onUserCreated(UserEvent $event){}

    /**
     * @param UserEvent $event
     */
    public function onUserLogin(UserEvent $event){}

    /**
     * @param UserEvent $event
     */
    public function onUserUpdated(UserEvent $event){}

    /**
     * @param UserEvent $event
     */
    public function onUserRemoved(UserEvent $event){}
}