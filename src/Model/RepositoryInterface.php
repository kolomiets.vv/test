<?php

namespace App\Model;

/**
 * Interface RepositoryInterface
 * @package App\Model
 */
interface RepositoryInterface
{
    const SORT_KEY        = 'sort_by';
    const SORT_ORDER      = 'sort_order';
    const FILTER_KEY      = 'filter';
    const FILTER_FORM_KEY = 'filter_form';
}