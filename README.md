# Test App 

### Description

- I have not enough time to wrap it in docker env for tests. On local env I have used 
php 7.1, nginx:1.13-alpine, mongo:4.0 docker images.
- Symfony version: 4.2
- For user registration/login fosUserBundle was used and configured.
- Test user creation can be done via command  "php bin/console fos:user:create testuser test@example.com p@ssword"
- Assign admin role to user via "php bin/console fos:user:promote testuser ROLE_ADMIN" command.
- Session as discussed stored in separated DB.
- Simple user CRUD system was realized.

### Extra bundles
- fosUserBundle
- monolog loger
- SensioFrameworkExtraBundle
- doctrine/mongodb-odm